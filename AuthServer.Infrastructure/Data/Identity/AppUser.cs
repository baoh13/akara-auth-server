﻿using Microsoft.AspNetCore.Identity;
using System;

namespace AuthServer.Infrastructure.Data.Identity
{
    public class AppUser : IdentityUser
    {
        // Add additional profile data for application users by adding properties to this class
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string JobTitle { get; set; }
        public string Organization { get; set; }
        public string LinkedIn { get; set; }
        public string About { get; set; }
        public int Type { get; set; }
        public DateTime CreatedBy { get; set; }
    }
}
