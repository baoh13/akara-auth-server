﻿using System.ComponentModel.DataAnnotations;

namespace AuthServer.Models
{
    public class UpdateProfileRequestModel
    {
        [Required]
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string JobTitle { get; set; }
        public string Organization { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Website { get; set; }
        public string About { get; set; }
    }
}
